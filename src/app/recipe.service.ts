import { Injectable } from '@angular/core';
import { Recipe } from './recipes/recipe.interface';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  private recipesList: Recipe[] = [
    {
      id: 'r1',
      title: 'Schnitzel',
      imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Schnitzel.JPG/1024px-Schenitzel.JPG',
      ingredients: ['French Fries','Pork Meat','Salad']
    },
    {
      id: 'r2',
      title: 'Spaghetti',
      imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Schnitzel.JPG/1024px-Schenitzel.JPG',
      ingredients: ['Spaguetti','Meat','Tomates']
    }
  ]

  constructor() { }

  getAll() {
    console.log("getAll",this.recipesList);
    return [...this.recipesList]
  }

  getRecipe(recipeId: string) {
    return {...this.recipesList.find(recipe => recipe.id === recipeId)}
  }

  deleteRecipe(recipeId: string) {
    this.recipesList = this.recipesList.filter(recipe => recipe.id !== recipeId )
    console.log(this.recipesList)
  }
}
