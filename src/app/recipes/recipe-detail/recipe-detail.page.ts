import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from '../../recipe.service';
import { Recipe } from '../../recipes/recipe.interface';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.page.html',
  styleUrls: ['./recipe-detail.page.scss'],
})
export class RecipeDetailPage implements OnInit {

  recipe: Recipe

  constructor(private activateRoute: ActivatedRoute,
    private _recipeService: RecipeService,
    private router: Router,
    private alertCtrl: AlertController) { }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(paramMap => {
      if (!paramMap.has('recipeId')) {
        //redirect
      }
      else {
        const recipeId = paramMap.get('recipeId')
        this.recipe = this._recipeService.getRecipe(recipeId)
      }
    }
    )
  }

  onDeleteRecipe() {
    console.log(this.recipe.id)
    this.alertCtrl.create({ header: 'Are you sure?',message: 'Do you really want to delete this recipe?', 
    buttons: [
      {
      text:'Cancel',
      role:'cancel'
      },
      {
        text: 'Delete',
        handler: () => {
          this._recipeService.deleteRecipe(this.recipe.id)
          this.router.navigate(['/recipes'])
        }
      }
    ] 
  }).then(alertEl => {
    alertEl.present()
  })

  }

}
