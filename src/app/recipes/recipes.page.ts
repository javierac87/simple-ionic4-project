import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../recipe.service';
import { Recipe } from './recipe.interface';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.page.html',
  styleUrls: ['./recipes.page.scss'],
})
export class RecipesPage implements OnInit {
  recipes: Recipe[] = []

  constructor(private _recipeService: RecipeService) { }

  ngOnInit() {

  }

  ionViewWillEnter () {
    this.recipes = this._recipeService.getAll();
    console.log("Recipes",this.recipes);
  }

}
